# SmartParkingLot

#### 介绍
项目名称：智能停车管理系统
项目功能：
 客户端：
	1、用户通过手机客户端注册登录
	2、客户端实时显示停车场内车位信息
	3、用户可通过客户端直接预约空车位
	4、用户通过手机客户端提示准确停入空车位
	5、用户取车后可通过客户端云端缴纳停车费用
 服务器端：
 	1、智能车牌识别
	2、实时显示当前停车场车位信息
	3、智能分配空车位给车主，并规划最佳路线引导车主停车
	4、自动计时收费
	5、实时更新数据库和日志文件，便于管理员管理停车场

#### 软硬件架构
硬件平台：
 服务器端：
 	ARM Cortex-A9  核心板：fs4412
 客户端：安卓设备
软件平台：
 qt编程，网络编程，c/c++ 。。。

#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. 网络协议：tcp
2. ip 192.168.1.200
3. 端口号：8888
4. 命令：使用枚举类型分类,发送时转换成字符串放在数据包头,命令和数据用":"分开,数据不止一个时用"空格"分开

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
