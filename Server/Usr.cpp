#include "Usr.h"
#include <QDebug>

Usr::Usr(QString name)
{
    this->Name = name;
}

QString Usr::getName() const
{
    return Name;
}

void Usr::setName(const QString &value)
{
    Name = value;
}

QString Usr::getPhoneNum()
{
    return this->PhoneNum;
}

void Usr::setPhoneNum(const QString &PhoneNum)
{
    if (PhoneNum.length() != PhoneNumSize) {
        //qDebug() << "手机号格式输入错误!" << endl;
        throw("手机号格式输入错误!");
    }

    int i = 0;
    while (i < PhoneNumSize) {
        if (!PhoneNum.at(i).isNumber()) {
            //qDebug() << "手机号格式输入错误!" << endl;
            throw("手机号格式输入错误!");
        }
        i++;
    }
    this->PhoneNum = PhoneNum;
}

QString Usr::getPassword()
{
    return this->Password;
}

void Usr::setPassword(const QString &Password)
{
    if (Password.length() >= PasswordSizeMax) {
        throw("密码长度过长!");
    }

    this->Password = Password;
}


