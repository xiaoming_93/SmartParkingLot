#include "Car.h"
#include <QDebug>

Car::Car(QString CarNum, bool Reservation)
{
    this->CarNum = CarNum;
    this->Reservation = Reservation;

    /* 还未计算费用赋值为-1 */
    this->Cost = -1;

    /* 支付状态为未支付 */
    this->PaymentStatus = false;
}

QString Car::getCarNum() const
{
    if (this->CarNum.isEmpty()) {
        return "-";
    }
    return CarNum;
}

QString Car::getEntryTime() const
{
    if (this->EntryTime.isEmpty()) {
        return "-";
    }
    return EntryTime;
}

void Car::setEntryTime(const QDateTime &value)
{
    this->EntryTime = value.toString("yyyy年MM月dd日hh:mm:ss");
}

void Car::setEntryTime(const QString &value)
{
    EntryTime = value;
}

void Car::setLeaveTime(const QDateTime &value)
{
    this->LeaveTime = value.toString("yyyy年MM月dd日hh:mm:ss");
}

void Car::setLeaveTime(const QString &value)
{
    LeaveTime = value;
}

QString Car::getLeaveTime() const
{
    if (this->LeaveTime.isEmpty()) {
        return "-";
    }
    return LeaveTime;
}

double Car::getCost() const
{
    return this->Cost;
}

void Car::setCost(int price)
{
    QString str = "yyyy年MM月dd日hh:mm:ss";
    QDateTime t1 = QDateTime::fromString(this->LeaveTime, str);
    QDateTime t2 = QDateTime::fromString(this->EntryTime, str);

    this->ParkingTime = QDateTime::fromMSecsSinceEpoch(
                t1.toMSecsSinceEpoch() - t2.toMSecsSinceEpoch()
                ).toUTC().toString("hh小时mm分钟ss秒");

    uint second = QDateTime::fromString(this->ParkingTime,"yyyy年MM月dd日hh:mm:ss").toTime_t();
    uint minute = second / 60;
    double hour = (double)minute / 60;
    this->Cost = hour * (double)price;
}

void Car::setCost(double value)
{
    this->Cost = value;
}

bool Car::getPaymentStatus() const
{
    return this->PaymentStatus;
}

void Car::setPaymentStatus(bool value)
{
    PaymentStatus = value;
}

bool Car::getReservation() const
{
    return Reservation;
}

void Car::setReservation(bool value)
{
    Reservation = value;
}

QString Car::getParkingTime() const
{
    if (this->ParkingTime.isEmpty()) {
        return "-";
    }
    return ParkingTime;
}

void Car::setParkingTime(const QDateTime &value)
{
    this->ParkingTime = value.toString("hh小时mm分钟");
}

void Car::setParkingTime(const QString &value)
{
    ParkingTime = value;
}

void Car::setCarNum(const QString &value)
{
    CarNum = value;
}






