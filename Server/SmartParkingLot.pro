#-------------------------------------------------
#
# Project created by QtCreator 2019-03-25T08:32:23
#
#-------------------------------------------------

QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SmartParkingLot
TEMPLATE = app


SOURCES += main.cpp\
        Widget.cpp \
    Car.cpp \
    Management.cpp \
    Usr.cpp

HEADERS  += Widget.h \
    Car.h \
    Management.h \
    Usr.h

FORMS    += Widget.ui

RESOURCES += \
    pic.qrc

DISTFILES +=
