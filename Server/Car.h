#ifndef CAR_H
#define CAR_H

#include <QString>
#include <QDateTime>

class Car
{
public:
    Car(QString CarNum = "null", bool Reservation = false);

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: getCarNum
    * 函数功能: 获取车牌号
    * 函数参数: void
    * 函数返回值: QString: 返回的车牌号
    *
    ***************************************/
    QString getCarNum() const;

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: setCarNum
    * 函数功能: 设置车牌号
    * 函数参数: const QString &value: 将被赋值给本类的成员变量
    * 函数返回值: void
    *
    ***************************************/
    void setCarNum(const QString &value);

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: getEntryTime
    * 函数功能: 获取车辆进去停车场的时间
    * 函数参数: void
    * 函数返回值: QString: 返回进入时间,为空返回"-"
    *
    ***************************************/
    QString getEntryTime() const;

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: setEntryTime
    * 函数功能: 设置进入时间,通过QDateTime类对象设置
    * 函数参数: const QDateTime &value: QDateTime类对象,将会把时间值转换为QString赋值给本类成员EntryTime
    * 函数返回值: void
    *
    ***************************************/
    void setEntryTime(const QDateTime &value);
    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: setEntryTime
    * 函数功能: 设置进入时间,直接通过QString设置
    * 函数参数: const QString &value: 将会被赋值给EntryTime
    * 函数返回值: void
    *
    ***************************************/
    void setEntryTime(const QString &value);

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: getLeaveTime
    * 函数功能: 获取离开时间
    * 函数参数: void
    * 函数返回值: QString: 返回离开时间,为空返回"-"
    *
    ***************************************/
    QString getLeaveTime() const;

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: setLeaveTime
    * 函数功能: 设置离开时间,通过QDateTime类对象设置
    * 函数参数: const QDateTime &value: QDateTime类对象,将会把时间值转换为QString赋值给本类成员LeaveTime
    * 函数返回值: void
    *
    ***************************************/
    void setLeaveTime(const QDateTime &value);

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: setLeaveTime
    * 函数功能: 设置离开时间,直接通过QString设置
    * 函数参数: const QString &value: 将会被赋值给LeaveTime
    * 函数返回值: void
    *
    ***************************************/
    void setLeaveTime(const QString &value);


    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: getLeaveTime
    * 函数功能: 获取停留时间
    * 函数参数: void
    * 函数返回值: QString: 返回停留时间,为空返回"-"
    *
    ***********************getLeaveTimegetParkingTime****************/
    QString getParkingTime() const;

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: setParkingTime
    * 函数功能: 设置停留时间,通过QDateTime类对象设置
    * 函数参数: const QDateTime &value: QDateTime类对象,将会把时间值转换为QString赋值给本类成员ParkingTime
    * 函数返回值: void
    *
    ***************************************/
    void setParkingTime(const QDateTime &value);

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: setParkingTime
    * 函数功能: 设置停留时间,直接通过QString设置
    * 函数参数: const QString &value: 将会被赋值给ParkingTime
    * 函数返回值: void
    *
    ***************************************/
    void setParkingTime(const QString &value);

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: getCost
    * 函数功能: 获取停车费用,
    * 函数参数: void
    * 函数返回值: double: 成功返回费用值,为空返回-1
    *
    ***************************************/
    double getCost() const;

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: setCost
    * 函数功能: 设置费用,通过收费标准设置费用
    * 函数参数: int price: 每小时收费标准
    * 函数返回值: void
    *
    ***************************************/
    void setCost(int price);

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: setCost
    * 函数功能: 设置费用,直接设置
    * 函数参数: double value: 将被赋值给本类的Cost成员变量
    * 函数返回值: void
    *
    ***************************************/
    void setCost(double value);

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: getPaymentStatus
    * 函数功能: 获取支付状态
    * 函数参数: void
    * 函数返回值: bool: 已支付返回true,未支付返回false
    *
    ***************************************/
    bool getPaymentStatus() const;

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: setPaymentStatus
    * 函数功能: 设置支付状态
    * 函数参数: bool value: 将被赋值给本类成员变量PaymentStatus
    * 函数返回值: void
    *
    ***************************************/
    void setPaymentStatus(bool value);

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: getReservation
    * 函数功能: 获取预约状态
    * 函数参数: void
    * 函数返回值: bool: 预约返回true,非预约返回false
    *
    ***************************************/
    bool getReservation() const;

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: setReservation
    * 函数功能: 设置预约状态
    * 函数参数: bool value: 将被赋值给本类成员变量Reservation
    * 函数返回值: void
    *
    ***************************************/
    void setReservation(bool value);


private:
    QString CarNum;         //车牌号
    QString EntryTime;      //进入时间
    QString LeaveTime;      //离开时间
    QString ParkingTime;    //停放时间
    double Cost;            //费用
    bool PaymentStatus;     //支付状态
    bool Reservation;       //是否为预约

};

#endif // CAR_H
