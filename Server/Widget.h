#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTcpServer>
#include <QtNetwork>
#include "Car.h"
#include "Usr.h"
#include "Management.h"

#define PRICE   2 //设置单价2元每小时
#define PORT    8888
#define MAXLEN  sizeof(int)

/* 定义所有命令枚举 */
enum {
    /* 登录,注册,预约,支付,注销,成功(所有操作的成功标志),未找到用户,密码错误,显示字符串*/
    LOGIN, JOIN, PAY, DELETE_USR, RESERVATION, SUCCESS, NOT_FOUNT_USR, PASSWORD_ERR, SHOW,
};

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

public slots:

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: connectSlot
    * 函数功能: 处理连接请求的槽函数
    * 函数参数: void
    * 函数返回值: void
    *
    ***************************************/
    void connectSlot();

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: recvInfoSlot
    * 函数功能: 接收并处理客户端命令和数据的槽函数
    * 函数参数: void
    * 函数返回值: void
    *
    ***************************************/
    void recvInfoSlot();

private:
    Ui::Widget *ui;
    QTcpServer *Server;
    QTcpSocket *Socket;
    Management *man;

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: sendInfo
    * 函数功能: 发送命令和数据
    * 函数参数:
    *   int cmd: 命令
    *   const QString &data: 数据,可不关注
    * 函数返回值: void
    *
    ***************************************/
    void sendInfo(int cmd, const QString &data = "");

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: updateUI
    * 函数功能: 更新ui界面显示信息
    * !注意事项: 每次更新显示前请手动调用 "man->setCar()" 函数!!!
    * 函数参数: void
    * 函数返回值: void
    *
    ***************************************/
    void updateUI();

};

#endif // WIDGET_H
