#ifndef MANAGEMENT_H
#define MANAGEMENT_H

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QFile>
#include "Car.h"
#include "Usr.h"

/* 移植前需要修改 */
#define SQL  "../Server/SQL/parking.db"                    //定义数据库相对路径
#define EmptySpaceTable "../Server/SQL/.EmptyLotTable.txt" //定义空车位表相对路径
#define LotNumMax 10                                       //定义总车位个数

typedef unsigned long u_long;

class Management
{
public:

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月27日
    * 函数名称: Management
    * 函数功能: 管理类的构造函数,初始化成员变量,初始化数据库,初始化空车位表文件,
    * 函数参数:
    *   const Car &car:
    *       Car类对象,将会被赋值给本类的成员car
    *   const Usr &usr:
    *       Usr类对象,将会被赋值给本类的成员usr
    * 函数返回值: 无
    *
    ***************************************/
    Management(const Car &car, const Usr &usr);

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月27日
    * 函数名称: Management
    * 函数功能: 本类的空构造函数,默认初始化成员
    * 函数参数: void
    * 函数返回值: 无
    *
    ***************************************/
    Management();

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月27日
    * 函数名称: ~Management
    * 函数功能: 本类的析够函数,关闭数据库,delete本类的成员指针变量
    * 函数参数: void
    * 函数返回值: 无
    *
    ***************************************/
    ~Management();

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月26日
    * 函数名称: AddCarToDatabase
    * 函数功能: 添加本类成员car信息到数据库
    * 函数参数: void
    * 函数返回值: void
    *
    ***************************************/
    void AddCarToDatabase();

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月26日
    * 函数名称: AddCarToDatabase
    * 函数功能: 添加car信息到数据库,AddCarToDatabase()的重载函数
    * 函数参数: Car &car: 需要添加到数据库的Car类对象
    * 函数返回值: void
    *
    ***************************************/
    void AddCarToDatabase(Car &car);

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月27日
    * 函数名称: DeleteCarFromDatabase
    * 函数功能: 从数据库删除指定车牌号的车辆信息
    * 函数参数: const QString &CarNum: 被删除车辆的车牌号
    * 函数返回值: 成功返回true,失败返回false
    *
    ***************************************/
    bool DeleteCarFromDatabase(const QString &CarNum);

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月27日
    * 函数名称: DeleteUsrFromDatabase
    * 函数功能: 从数据库删除指定手机号的用户信息
    * 函数参数: const QString &PhoneNum: 被删除用户的手机号
    * 函数返回值: 成功返回true,失败返回false
    *
    ***************************************/
    bool DeleteUsrFromDatabase(const QString &PhoneNum);

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月26日
    * 函数名称: AddUsrToDatabase
    * 函数功能: 添加本类成员usr信息到数据库
    * 函数参数: void
    * 函数返回值: void
    *
    ***************************************/
    void AddUsrToDatabase();

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月26日
    * 函数名称: AddUsrToDatabase
    * 函数功能: 添加usr信息到数据库,AddUsrToDatabase()的重载函数
    * 函数参数: const Usr &usr: 需要添加的Usr类对象
    * 函数返回值: void
    *
    ***************************************/
    void AddUsrToDatabase(Usr &usr);

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月27日
    * 函数名称: QueryCarFromDatabase
    * 函数功能: 通过车牌号从数据库中查询车辆
    * 函数参数:
    *   const QString &CarNum: 被查询的车牌号
    *   bool *ok: 用于保存是否查询成功,不关注是可忽略或传Q_NULLPTR
    * 函数返回值:
    *   Car: 返回查询到的结果,有可能查询失败,建议使用ok参数判断
    *
    ***************************************/
    Car QueryCarFromDatabase(const QString &CarNum, bool *ok = Q_NULLPTR);

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月27日
    * 函数名称: QueryUsrFromDatabase
    * 函数功能: 通过手机号查询用户
    * 函数参数:
    *   const QString &PhoneNum: 被查询的手机号
    *   bool *ok: 用于保存是否查询成功,不关注是可忽略或传Q_NULLPTR
    * 函数返回值:
    *   Car: 返回查询到的结果,有可能查询失败,建议使用ok参数判断
    *
    ***************************************/
    Usr QueryUsrFromDatabase(const QString &PhoneNum, bool *ok = Q_NULLPTR);

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月27日
    * 函数名称: getEmptySpace
    * 函数功能: 获取一个空车位的车位号,并更新当前类的EmptySpaceArr成员,将会自动调用setEmptySpaceNum()函数,自动调用UpdateFile()函数
    * 函数参数: void
    * 函数返回值: 成功返回获取的车位号,失败返回0失败表明没有空车位
    *
    ***************************************/
    u_long getEmptySpace();

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月27日
    * 函数名称: FreeParkingSpace
    * 函数功能: 释放一个车位,并更新当前类的EmptySpaceArr成员,将会自动调用setEmptySpaceNum()函数,自动调用UpdateFile()函数
    * 函数参数:
    *   const u_long &value:
    *       被释放的车位号,将会被插入到EmptySpaceArr中,保证EmptySpaceArr有序,
    * 函数返回值: bool: 成功返回true,失败返回false,失败说明停车场内没有停入车辆
    *
    ***************************************/
    bool FreeParkingSpace(const u_long &value);

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: UpdateCarFromDatabase
    * 函数功能: 更新车辆信息,将本类的成员car更新到数据库中,修改数据库
    * 函数参数: void
    * 函数返回值: bool: 成功返回true,失败返回false
    *
    ***************************************/
    bool UpdateCarFromDatabase();

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: UpdateCarFromDatabase
    * 函数功能: 更新车辆信息,将参数car更新到数据库中,修改数据库
    * 函数参数: Car &car: 待更改的车辆
    * 函数返回值: bool: 成功返回true,失败返回false
    *
    ***************************************/
    bool UpdateCarFromDatabase(Car &car);

    Car getCar() const;
    void setCar(const Car &value);

    Usr getUsr() const;
    void setUsr(const Usr &value);

    u_long getEmptySpaceNum() const;


private:

    /* 车辆类 */
    Car car;

    /* 用户类,手机客户端注册的用户 */
    Usr usr;

    /* 数据库句柄 */
    QSqlDatabase db;

    /* 数据库查询类 */
    QSqlQuery query;

    /* 文件类 */
    /* 用于操作储存当前空车位信息的文件 */
    QFile *File;

    /* 空车位表 */
    u_long EmptySpaceArr[LotNumMax];

    /* 空车位个数 */
    u_long EmptySpaceNum;

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月27日
    * 函数名称: setEmptySpaceNum
    * 函数功能: 本类的私有函数,仅供内部使用,设置(更新)当前类的成员变量EmptySpaceNum,
    * 函数参数: void
    * 函数返回值: void
    *
    ***************************************/
    void setEmptySpaceNum();

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月27日
    * 函数名称: UpdateFile
    * 函数功能: 本类的私有函数,仅供内部使用,更新空车位表文件
    * 函数参数: void
    * 函数返回值: void
    *
    ***************************************/
    void UpdateFile();

};

#endif // MANAGEMENT_H
