#include "Widget.h"
#include "ui_Widget.h"
#include <QDateTime>
#include <QDebug>
#include <QMovie>


Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    /* 取消边框 */
    this->setWindowFlags(Qt::FramelessWindowHint);

    /* 设置欢迎动画 */
    QMovie *movie = new QMovie(":/welcome.gif");
    ui->WelcomeBackgroung->setMovie(movie);
    movie->start();

    /* 初始化管理类 */
    this->man = new Management;

    this->updateUI();

    /* 初始化Tcp服务器 */
    this->Server = new QTcpServer;

    /* 初始化套接字 */
    this->Socket = new QTcpSocket;

    /* 监听任意主机 */
    this->Server->listen(QHostAddress::Any, PORT);

    /* 关联接收连接槽函数 */
    this->connect(Server, SIGNAL(newConnection()),this, SLOT(connectSlot()));

#if 0
    Car car("陕A-88888");
    Usr usr("yu");
    usr.setPhoneNum("18797329441");
    Management m(car, usr);
    Car car1 = m.QueryCarFromDatabase(car.getCarNum());
    qDebug() << car1.getCarNum() << endl;
    qDebug() << car1.getCost() << endl;
    qDebug() << car1.getEntryTime() << endl;
    qDebug() << car1.getLeaveTime() << endl;
    qDebug() << car1.getParkingTime() << endl;
    qDebug() << car1.getPaymentStatus() << endl;
    qDebug() << car1.getReservation() << endl;
    Usr usr1 = m.QueryUsrFromDatabase(usr.getPhoneNum());
    qDebug() << usr1.getName() << endl;
    qDebug() << usr1.getPhoneNum() << endl;
    qDebug() << usr1.getPassword() << endl;
#endif

}

Widget::~Widget()
{
    delete ui;
    delete this->Server;
    delete this->Socket;
    delete this->man;

}

void Widget::connectSlot()
{
    while (this->Server->hasPendingConnections()) {
        this->Socket = this->Server->nextPendingConnection();
        qDebug() << "123" << endl;
        this->connect(this->Socket, SIGNAL(readyRead()), this, SLOT(recvInfoSlot()));
    }
}

void Widget::recvInfoSlot()
{
    /* 读取所有数据 */
    QString str = this->Socket->readAll();
    qDebug() << str << endl;

    /* 截取字符串 */
    QStringList list = str.split(":");

    /* 获取命令 */
    QString cmd = list.at(0);
    qDebug() << "命令:" << cmd << endl;

    /* 获取数据 */
    QString data = list.at(1);
    qDebug() << "数据:" << data << endl;

    qDebug() << "测试switch1" << endl;
    u_long num = cmd.toInt();
    qDebug() <<  num << endl;
    switch (num) {

    /* 登录 */
    case LOGIN:
    {
        qDebug() << "登录" << endl;
        QStringList s = data.split(" ");
        QString PhoneNumStr = s.at(0);
        QString PasswordStr = s.at(1);
        bool ok;
        Usr usr = this->man->QueryUsrFromDatabase(PhoneNumStr, &ok);
        if (!ok) {

            /* 未找到用户 */
            this->sendInfo(NOT_FOUNT_USR);

        }
        if (usr.getPassword() == PasswordStr) {

            /* 登录成功 */
            this->sendInfo(SUCCESS);
        }

        /* 密码错误 */
        this->sendInfo(PASSWORD_ERR);
    }break;

    /* 注册 */
    case JOIN:
    {
        qDebug() << "注册" << endl;
        QStringList s = data.split(" ");
        QString UsrNameStr  = s.at(0);
        QString PhoneNumStr = s.at(1);
        QString PasswordStr = s.at(2);

        Usr usr(UsrNameStr);
        usr.setPhoneNum(PhoneNumStr);
        usr.setPassword(PasswordStr);
        this->man->AddUsrToDatabase(usr);

        this->sendInfo(SUCCESS);

    }break;

    /* 预约 */
    case RESERVATION:
    {
        qDebug() << "预约" << endl;
        /* 设置预约车辆 */
        Car newCar(data);
        newCar.setReservation(true);
        this->man->setCar(newCar);

        /* 将预约车辆添加到数据库 */
        this->man->AddCarToDatabase();

        /* 获取空车位 */
        u_long num = this->man->getEmptySpace();

        /* 发送数据到客户端 */
        this->sendInfo(SUCCESS, QString::number(num));
    }break;

    /* 支付 */
    case PAY:
    {
        qDebug() << "支付" << endl;
        Car newCar = this->man->getCar();
        newCar.setPaymentStatus(true);
        this->man->UpdateCarFromDatabase(newCar);
    }
    /* 注销 */
    case DELETE_USR:
    {
        qDebug() << "注销" << endl;
        this->man->DeleteUsrFromDatabase(data);
    }break;
    default:
        qDebug() << "测试default" << endl;
        break;
    }

}

void Widget::sendInfo(int cmd, const QString &data)
{
    QString str = QString::number(cmd);
    str += ":";
    str += data;
    this->Socket->write(str.toUtf8());
}

void Widget::updateUI()
{
    /* ui界面显示 */
    ui->EmptySpaceNum->display((int)this->man->getEmptySpaceNum());
    ui->CarNum->setText(man->getCar().getCarNum());
    ui->Cost->setText(man->getCar().getCost() < 0 ? "-" : QString::number(man->getCar().getCost()));
    ui->EntryTime->setText(man->getCar().getEntryTime());
    ui->LeaveTime->setText(man->getCar().getLeaveTime());
    ui->ParkingTime->setText(man->getCar().getParkingTime());
    ui->PaymentStatus->setText(man->getCar().getPaymentStatus()?"已支付":"未支付");

    this->update();
}
