#ifndef USR_H
#define USR_H

#include <QString>

#define PhoneNumSize    11  //定义手机号位数
#define PasswordSizeMax 20  //定义密码最大长度

class Usr
{
public:
    Usr(QString name = "usr");

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: getName
    * 函数功能: 获取用户名
    * 函数参数: void
    * 函数返回值: QString: 直接返回本类成员Name的值
    *
    ***************************************/
    QString getName() const;

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: setName
    * 函数功能: 设置用户名
    * 函数参数: const QString &value: 将被赋值给本类成员Name
    * 函数返回值: void
    *
    ***************************************/
    void setName(const QString &value);

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: getPhoneNum
    * 函数功能: 获取手机号
    * 函数参数: void
    * 函数返回值: QString: 直接返回本类成员PhoneNum的值
    *
    ***************************************/
    QString getPhoneNum();

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: setPhoneNum
    * 函数功能: 设置手机号
    * 函数参数: const QString &PhoneNum: 将被赋值给本类成员PhoneNum
    * 函数返回值: void
    *
    ***************************************/
    void setPhoneNum(const QString &PhoneNum);

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: getPassword
    * 函数功能: 获取密码
    * 函数参数: void
    * 函数返回值: QString: 将本类成员Password的值返回
    *
    ***************************************/
    QString getPassword();

    /**************************************
    * 作者: 勇哥
    * 日期: 2019年3月28日
    * 函数名称: setPassword
    * 函数功能: 设置密码
    * 函数参数: const QString &Password: 将会被赋值给本类成员Password
    * 函数返回值: void
    *
    ***************************************/
    void setPassword(const QString &Password);

private:

    QString Name;       //用户名
    QString PhoneNum;   //手机号
    QString Password;   //密码
};

#endif // USR_H
